using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {   

        float dirX = Input.GetAxis("Horizontal");
        float dirY = Input.GetAxis("Vertical");
        
        rb.velocity = new Vector2(dirX * 4f, rb.velocity.y);
        rb.velocity = new Vector2(dirY * 4f, rb.velocity.x);

      /*  if(Input.GetButton("Jump")){
            rb.velocity = new Vector2(rb.velocity.x, 1f);
        }*/
    }
}
